# vs3-model
<!-- badges: start -->
[![Travis build status](https://travis-ci.com/thomasmotal/vs3model.svg?branch=master)](https://travis-ci.com/thomasmotal/vs3model)
[![AppVeyor build status](https://ci.appveyor.com/api/projects/status/github/thomasmotal/vs3model?branch=master&svg=true)](https://ci.appveyor.com/project/thomasmotal/vs3model)
[![Codecov test coverage](https://codecov.io/gh/thomasmotal/vs3model/branch/master/graph/badge.svg)](https://codecov.io/gh/thomasmotal/vs3model?branch=master)
<!-- badges: end -->

VS3 statistics model for american football
